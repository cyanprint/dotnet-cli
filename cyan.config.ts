import {Chalk} from "chalk";
import {Inquirer} from "inquirer";
import {Cyan, Documentation, DocUsage, Glob, IAutoInquire, IAutoMapper, IExecute,} from "./Typings";

export = async function (folderName: string, chalk: Chalk, inquirer: Inquirer, autoInquirer: IAutoInquire, autoMap: IAutoMapper, execute: IExecute): Promise<Cyan> {
	let ide: any = {
		vs2017: "VS 2017",
		webStorm: "Rider",
		editor: "Text Editor (VS Code, Sublime, Atom etc)"
	};
	
	let ideAnswer: object = await autoInquirer.InquireAsList(ide, "Which IDE do you use?");
	let ideChoice: string = autoMap.ReverseLoopUp(ide, ideAnswer);
	
	let globs: Glob[] = [];
	
	let ideIgnore: { vs2017: string[], webStorm: string[] } = {
		vs2017: ["**/.idea/**/*", "**/.idea", "**/obj/**/*", "**/.vs/**/*", "**/bin/**/*"],
		webStorm: ["**/obj/**/*", "**/bin/**/*", "**/*.njsproj", "**/*.njsproj.user", "**/.vs/**/*"]
	};
	
	//Pick files base on IDE
	if (ideChoice === ide.webStorm) {
		globs.push({root: "./Template/", pattern: "**/*.*", ignore: ideIgnore.webStorm})
	} else if (ideChoice === ide.vs2017) {
		globs.push({root: "./Template/", pattern: "**/*.*", ignore: ideIgnore.vs2017});
	} else {
		globs.push({
			root: "./Template/",
			pattern: "**/*.*",
			ignore: ideIgnore.vs2017.concat(ideIgnore.webStorm)
		})
	}
	//Ask for documentation
	let docQuestions: DocUsage = {license: false, git: false, readme: true, semVer: false, contributing: false};
	let docs: Documentation = await autoInquirer.InquireDocument(docQuestions);
	
	//Get read question to ask
	let variables: any = {
		binary: ["cli", "Enter the name of binary (name of your command)"]
	};
	
	//Get answers
	variables = await autoInquirer.InquireInput(variables);
	
	//Fill in empty information if document already have them
	variables.description = docs.data.description;
	if (docs.usage.git) variables.git = docs.data.gitURL;
	
	let flags: any = {
		xunit: "xUnit",
		nunit: "nUnit"
	};
	
	let unittest = await autoInquirer.InquirePredicate("Do you want unit test?");
	if (unittest) {
		flags = await autoInquirer.InquireAsList(flags, "Which unit test framewok do you want to use?");
	} else {
		flags.nunit = false;
		flags.xunit = false;
	}
	
	let guid: string[] = ["F676D4DA-361E-4BD3-B013-93C2B1446FC3", "2ECDDA5D-A74C-4DAF-AA92-F44678DC6330"];
	if (flags.nunit) guid.push("8934FCB6-007C-4F57-BBB3-55ED6145A80C");
	if (flags.xunit) guid.push("3B78B3F5-27A9-4AF0-ACD0-7BA3059B06A2");
	
	return {
		globs: globs,
		flags: flags,
		variable: variables,
		guid: guid,
		docs: docs,
		comments: ["#", "//"]
	} as Cyan;
}
