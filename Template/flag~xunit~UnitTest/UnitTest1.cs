using main;
using System;
using Xunit;

namespace test
{
    public class TestProgram
    {
        [Fact]
        public void Check_if_string_reverses()
        {
            Assert.Equal("eennirik", Program.ReverseString("kirinnee"));
        }
    }
}
