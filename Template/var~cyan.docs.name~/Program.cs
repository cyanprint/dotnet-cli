﻿using System;
using System.Linq;

namespace main
{
    public class Program
    {
        private static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("var~cyan.docs.name~:\n" +
                    "\n" +
                    "var~description~" +
                    "\n" +
                    "Use var~binary~ --help to see list of commands\n");
                return;
            }
            if(args.Length == 1)
            {
                if(args[0] == "--help")
                {
                    Console.WriteLine("var~binary~ <message> : prints message in caps");
                }
                else
                {
                    Console.WriteLine(args[0].ToUpper());
                }
            }
        }

        public static string ReverseString(string x)
        {
            return string.Join("", x.ToCharArray().Reverse());
        }
    }
}
